var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ReedemapiProvider } from '../../providers/reedemapi/reedemapi';
import { LoadingController } from 'ionic-angular';
import { ModalpagePage } from '../modalpage/modalpage';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
var RedeemHistoryPage = /** @class */ (function () {
    function RedeemHistoryPage(navCtrl, navParams, alertCtrl, reedemapi, loadingCtrl, modalCtrl, storage, datepipe) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.reedemapi = reedemapi;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.datepipe = datepipe;
        this.offset = 0;
        this.limit = 20;
        this.items = [];
        this.setLoader();
        this.getReedemHistory();
    }
    RedeemHistoryPage_1 = RedeemHistoryPage;
    RedeemHistoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RedeemHistoryPage');
        console.log(document.getElementById('from_date'));
    };
    RedeemHistoryPage.prototype.setLoader = function () {
        this.load = this.loadingCtrl.create({
            content: 'Please Wait....',
        });
        this.load.present();
    };
    RedeemHistoryPage.prototype.getReedemHistory = function () {
        var _this = this;
        var date = new Date();
        var fromdate = this.datepipe.transform(date, 'yyyy-MM-dd');
        var todate = this.datepipe.transform(date, 'yyyy-MM-dd');
        if (this.modalData != undefined) {
            this.filterFlag = this.filterFlag + 1;
            fromdate = this.modalData['fromDate'];
            todate = this.modalData['toDate'];
            this.offset = 0;
            if (fromdate == undefined || todate == undefined) {
                fromdate = '';
                todate = '';
                this.filterFlag = 0;
                this.offset = 0;
            }
        }
        else {
            this.filterFlag = 0;
            fromdate = '';
            todate = '';
        }
        this.storage.get('api_token').then(function (token) {
            console.log('abcd');
            _this.items = [];
            console.log("abcd2");
            _this.reedemapi.getReedemHistory(token, _this.offset, fromdate, todate)
                .then(function (dataSet) {
                _this.load.dismiss();
                console.log(dataSet);
                var resp = dataSet;
                if (resp['total_count'] > 0) {
                    _this.historyShow = true;
                    if (resp['status'] == true) {
                        console.log(resp['data']);
                        _this.offset = _this.offset + 10;
                        var dataArray = resp['data'];
                        for (var i = 0; i < dataArray.length; i++) {
                            _this.items.push(dataArray[i]);
                        }
                        _this.totalCount = resp['total_count'];
                        console.log(_this.items);
                    }
                }
                else {
                    _this.historyShow = false;
                }
            }, function (error) {
                console.log(error);
            });
        });
    };
    RedeemHistoryPage.prototype.pushingList = function (e) {
        var _this = this;
        var date = new Date();
        var fromdate = this.datepipe.transform(date, 'yyyy-MM-dd');
        var todate = this.datepipe.transform(date, 'yyyy-MM-dd');
        if (this.modalData != undefined) {
            fromdate = this.modalData.fromDate;
            todate = this.modalData.toDate;
        }
        else {
            fromdate = '';
            todate = '';
        }
        console.log("in pushing");
        this.storage.get('api_token').then(function (token) {
            _this.reedemapi.getReedemHistory(token, _this.offset, fromdate, todate)
                .then(function (dataSet) {
                _this.load.dismiss();
                console.log(dataSet);
                var resp = dataSet;
                if (resp['status'] == true) {
                    console.log(resp['data']);
                    console.log(_this.totalCount);
                    if (_this.totalCount > _this.offset) {
                        _this.offset = _this.offset + 10;
                        var dataArray = resp['data'];
                        for (var i = 0; i < dataArray.length; i++) {
                            _this.items.push(dataArray[i]);
                        }
                    }
                    e.complete();
                    console.log(_this.items);
                }
            }, function (error) {
                console.log(error);
                e.complete();
            });
        });
    };
    RedeemHistoryPage.prototype.presentAlert = function () {
        /*let alert = this.alertCtrl.create({
           title: 'Dates',
           subTitle: '10% of battery remaining',
           inputs: [
               {
                 name: 'from Date',
                 placeholder: 'From Date',
                 type: 'date',
                 id: 'from_date'
               },
               {
               name: 'To date',
                 placeholder: 'To Date',
                 type: 'date'
               }
             ],
           buttons: ['Dismiss']
         });
         alert.present();*/
        var _this = this;
        var modal = this.modalCtrl.create(ModalpagePage, { page: RedeemHistoryPage_1 });
        modal.present();
        modal.onDidDismiss(function (data) {
            _this.modalData = data;
            if (_this.modalData != undefined) {
                if (_this.modalData.fromDate != '' && _this.modalData.toDate != '') {
                    console.log(_this.modalData.fromDate);
                    console.log(_this.modalData.toDate);
                    console.log(data);
                    console.log(_this.modalData['fromDate']);
                    console.log(_this.modalData['toDate']);
                }
            }
            _this.getReedemHistory();
        });
    };
    RedeemHistoryPage.prototype.doInfinite = function (e) {
        this.pushingList(e);
    };
    var RedeemHistoryPage_1;
    RedeemHistoryPage = RedeemHistoryPage_1 = __decorate([
        IonicPage(),
        Component({
            selector: 'page-redeem-history',
            templateUrl: 'redeem-history.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams,
            AlertController, ReedemapiProvider,
            LoadingController, ModalController,
            Storage, DatePipe])
    ], RedeemHistoryPage);
    return RedeemHistoryPage;
}());
export { RedeemHistoryPage };
//# sourceMappingURL=redeem-history.js.map