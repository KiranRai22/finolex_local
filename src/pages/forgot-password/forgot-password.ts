	import { Component } from '@angular/core';
	import { IonicPage, NavController, NavParams,LoadingController,AlertController } from 'ionic-angular';
	import {FormBuilder,Validators,FormGroup} from '@angular/forms';
	import {OtpPage} from '../otp/otp';
	import {ForgotpasswordProvider} from '../../providers/forgotpassword/forgotpassword';
	import {ConfirmpasswordPage} from '../confirmpassword/confirmpassword';
	import { Storage } from '@ionic/storage';

	@Component({
	  selector: 'page-forgot-password',
	  templateUrl: 'forgot-password.html',
	})
	export class ForgotPasswordPage {
		form:FormGroup;
		mobile_no: any;
		loader : any;
		data : any;

	  constructor(public navCtrl: NavController, public navParams: NavParams,private _form: FormBuilder,private forgotProvider : ForgotpasswordProvider,
	  	private loaderController : LoadingController,private storage: Storage,public alert : AlertController) {
	  	 this.form=this._form.group({
	        mobile_no:['',Validators.compose([Validators.required,Validators.maxLength(10),Validators.minLength(10)])]
	    });
	  }


	  ionViewDidLoad() {
	    console.log('ionViewDidLoad ForgotPasswordPage');
	  }

	   sendOtp(){

			  	 this.loader= this.loaderController.create({
			      content:'Please Wait....',
			    });
			    this.loader.present();

	        console.log(this.form.value['mobile_no']);

	        
			this.forgotProvider.getForgotPassword(this.form.value['mobile_no'])
		    .then(data => {

		    	this.loader.dismiss();

		    	if(data['status']==true){


		    		this.data={
              page:"ForgotPasswordPage",
              mobile_no : this.form.value['mobile_no']
      }

 					
 					this.navCtrl.push(OtpPage,{data:this.data});
		    	}
		    	else{
					let alertCtrl = this.alert.create({
					    title: 'Finolex',
					    subTitle: data['message'],
					    buttons: ['OK']
					  });
					  alertCtrl.present();		    	}
	    			console.log(data);
		    });
	    }

	}
