import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import {SetpasswordProvider} from '../../providers/setpassword/setpassword'
import {OtpPage} from '../otp/otp';
import { Storage } from '@ionic/storage';
import {TabsScreenPage} from '../tabs-screen/tabs-screen';
import { AlertController } from 'ionic-angular';


@Component({
  selector: 'page-confirmpassword',
  templateUrl: 'confirmpassword.html',
})
export class ConfirmpasswordPage {

	newPassword : any;
	confirmPassword : any;
 	oldpasswordType: string = 'password';
    oldpasswordIcon: string = 'eye-off'; 
    newpasswordType: string = 'password';
	newpasswordIcon: string = 'eye-off'; 
	confirmpasswordType: string = 'password';
	confirmpasswordIcon: string = 'eye-off';
	checkPass : boolean = true;
	isConfirm:boolean;
	mobileNumber : any;
	loader : any;role;

  constructor(public navCtrl: NavController, public navParams: NavParams,public setpasswordProvider : SetpasswordProvider,
 	public loadingController : LoadingController,private storage: Storage,private alertCtrl: AlertController) {
  	this.mobileNumber=this.navParams.get('mobileNumber');
    console.log('mobile',this.mobileNumber);
  	this.role=this.navParams.get('role');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmpasswordPage');
  }

  hideShowPassword(val) {

	  if(val=='New'){
	     this.newpasswordType = this.newpasswordType === 'text' ? 'password' : 'text';
	     this.newpasswordIcon = this.newpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
	  }
	  else if(val == 'Confirm'){
	     this.confirmpasswordType = this.confirmpasswordType === 'text' ? 'password' : 'text';
	     this.confirmpasswordIcon = this.confirmpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
	 	 }
 }

  validatePassword(){

  	if(this.newPassword==this.confirmPassword){
  	this.isConfirm=true;
  	this.checkPass=true;	
  	}
  	else{
  		this.isConfirm=false;
  		this.checkPass=false;
  	}
  }
  
  changePassword(){

		this.loader= this.loadingController.create({
					      content:'Please Wait....',
					    });
					    this.loader.present();
	this.storage.get('role').then(val=>{
		console.log(val);
  	this.setpasswordProvider.setNewPassword(this.mobileNumber,this.newPassword,this.confirmPassword,this.role)
		    .then(data => {
		    	this.loader.dismiss();
		    	if(data['status']==true){
 					
 		//		this.navCtrl.push(OtpPage,{mobileNumber:this.mobileNumber});
 		            this.presentAlert(data['message']);
 					this.navCtrl.push(TabsScreenPage); 		
		    	}
		    	else{
 		            this.presentAlert(data['message']);
		    	}
		    //	this.navCtrl.push(ConfirmpasswordPage);
	    	console.log(data);
		    });
		});
  }
  presentAlert(message) {
    let alert = this.alertCtrl.create({
      
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok clicked');
          }
        }
      ]
    });
    alert.present();
  }
 }
