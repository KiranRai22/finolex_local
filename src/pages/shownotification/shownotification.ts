import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-shownotification',
  templateUrl: 'shownotification.html',
})
export class ShownotificationPage {

 notification : any;
 notification_id : any;
 canLogin : boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public loadingCtrl: LoadingController,
    public user:UserserviceProvider,
  	private storage: Storage) {
  	 this.notification=navParams.get('notification');
  	 console.log('note',this.notification);
     this.notification_id=this.notification['user_notification_id'];

     this.storage.get('can_login').then((val) => {
          
          console.log('login',val);
          if(val==true){
            this.canLogin=true;
          }
          else{

            this.canLogin=false;
          }

        });

  	 this.loadData(); 


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShownotificationPage');
  }

loadData(){

    let loader = this.loadingCtrl.create({
        content : "Please wait..."
    });

    loader.present();
    this.storage.get('api_token').then((val)=>{
      
      loader.dismiss();
        this.user.sendNotificationStatus(val,this.notification_id).then(data2=>{

          console.log(data2);
          
          }).catch(err=>{      

            console.log(err);
        });
    });

  }

}
