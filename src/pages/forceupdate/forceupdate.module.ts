import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForceupdatePage } from './forceupdate';

@NgModule({
  declarations: [
    ForceupdatePage,
  ],
  imports: [
    IonicPageModule.forChild(ForceupdatePage),
  ],
})
export class ForceupdatePageModule {}
