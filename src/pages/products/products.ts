import { Component } from '@angular/core';
import { NavController,ModalController, Platform, NavParams, ViewController,LoadingController } from 'ionic-angular';
import { ProductsModalPage} from '../products-modal/products-modal';
import { ModalpagePage} from '../modalpage/modalpage';
import { AlertController } from 'ionic-angular';
import { ReedemapiProvider } from '../../providers/reedemapi/reedemapi';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {NotificationsPage} from '../notifications/notifications';

@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {

data : any;
products : any;
load : any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl : ModalController,public viewCtrl: ViewController,public alertCtrl: AlertController,public storage:Storage,
    public reedemapi : ReedemapiProvider,public loadingCtrl: LoadingController,public iab : InAppBrowser) {
	
      this.loaddata();
  }

    loaddata(){

         this.load= this.loadingCtrl.create({
          content:'Please Wait....',
        });
        this.load.present();

            this.storage.get('api_token').then(token=>{
                  
                  this.reedemapi.getProductCatalogues(token)
                    .then(data => {
                        this.load.dismiss();
                        console.log(data);
                        let resp = data;
                        if(resp['status'] == true){
                          console.log(resp['data']);
                          this.data = resp['data'];
                        }          
                 },error=>{
                      console.log(error);
                 });
              });    

    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad ProductsPage');
   
    }

    productClick(id){
    	console.log('item clicked',this.data[id]);
        this.navCtrl.push(ProductsModalPage,{productData:this.data[id]});
    }
    openBrowser(product){
      const browser = this.iab.create(product.link);
      browser.show();
    }

}
