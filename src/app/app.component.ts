import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { RedeemcodePage } from '../pages/redeemcode/redeemcode';
import { ProductsPage} from  '../pages/products/products';
import { Storage } from '@ionic/storage';
import { ChangeRewardPage } from '../pages/change-reward/change-reward';
import { RedeemHistoryPage } from '../pages/redeem-history/redeem-history';
import { RewardHistoryPage } from '../pages/reward-history/reward-history';
import { ProfilePage } from '../pages/profile/profile';
import { TabsScreenPage} from '../pages/tabs-screen/tabs-screen';
import { Firebase  } from '@ionic-native/firebase';
import { ForgotPasswordPage} from '../pages/forgot-password/forgot-password';
import { ChangePasswordPage} from '../pages/change-password/change-password';
import firebase from 'firebase';
import { Events,ToastController ,AlertController } from 'ionic-angular';
import { NotificationsPage} from '../pages/notifications/notifications';
import { ContactUsPage} from '../pages/contact-us/contact-us';
import { TranslateService } from '@ngx-translate/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { UserserviceProvider } from '../providers/userservice/userservice';
import {ForceupdatePage} from '../pages/forceupdate/forceupdate';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = TabsScreenPage;
  deviceToken;name;profilePic;
  pages: Array<{ title: string, component: any,icon: any }>;

  username;image;

  constructor(public platform: Platform,private firebaseIon: Firebase,
    private translateService: TranslateService,
    public statusBar: StatusBar, public splashScreen: SplashScreen,
    private storage : Storage,public events: Events,
    private toastCtrl: ToastController, public user:UserserviceProvider,
    public alert: AlertController, private appVersion : AppVersion,
    private market : Market) {
    this.initializeApp();

    let successCallback = (isAvailable) => { /*alert('Is available? ' + isAvailable);*/
      console.log('availbale',isAvailable);
    };
    let errorCallback = (e) => console.error(e);

    this.pages = [
      { title: 'Dashboard', component: DashboardPage, icon:'assets/icon/Dashboard.svg' },
      { title: 'Scan Code', component: RedeemcodePage, icon:'assets/icon/RedeemCode.svg' },
      { title: 'Change Reward Type', component: ChangeRewardPage, icon:'assets/icon/Change_Reward_Type.svg' },
      { title: 'Scan history', component: RedeemHistoryPage, icon:'assets/icon/RedeemHistory.svg' },
      { title: 'Reward History', component: RewardHistoryPage, icon:'assets/icon/RewardHistory.svg' },
      { title: 'Product Catalogue', component: ProductsPage, icon:'assets/icon/ProductCatalogue.svg' },
      { title: 'Profile', component: ProfilePage, icon:'assets/icon/Profile.svg' },
      { title: 'Change Password', component: ChangePasswordPage, icon:'assets/icon/ChangePassword_Final.svg'},
      { title: 'Contact Us', component: ContactUsPage, icon:'assets/icon/contact.svg'},
      { title: 'Logout', component: null, icon:'assets/icon/Logout.svg' },
    ];

    events.subscribe('user:language',(language)=>{
      if(language=="Hindi"){
        this.translateService.use('hi');
      }
      else{
        this.translateService.use('en');
      }
    });

    events.subscribe('user:created', (user, time) => {
    // user and time are the same arguments passed in `events.publish(user, time)`
      this.storage.get('user_name').then((val) => {
        this.username = val;
        this.storage.get('image').then((value)=>{
          this.image=value;
          if(this.image==null){
            this.image="assets/images/Blank_Avatar.png";
          }
        })
      });
    });
    
    this.firebaseIon.getToken()
    .then(token =>{
      this.storage.set('token',token);
    });

    this.storage.get('user_name').then((val) => {
         this.username = val;
    });


    if(this.platform.is('cordova') || this.platform.is('android')){
        
        this.firebaseIon.getToken().then(token =>{

              if(token != "" && token !=null){
                this.storage.set("deviceToken",token);
                 this.storage.get('deviceToken').then(deviceId=>{
                 });
                this.deviceToken = token;
              }
              this.firebaseIon.onNotificationOpen().subscribe((notification)=>{
               if(notification.tap != undefined && notification.tap == false ){
                 let toast = this.toastCtrl.create({
                  message: 'New Notification Sent',
                  duration: 3000,
                  position: 'top'
                });
                toast.present();
               }else{
                 this.nav.push(NotificationsPage);
               }
              });

          }).catch(error => {
            console.log(error);
          });
    }
  }

  initializeApp() {

    this.platform.ready().then(() => {
    this.translateService.setDefaultLang('en');
    this.storage.get("language").then((val)=>{
      if(val=="Hindi"){
        this.translateService.use('hi');
      }
      else{
        this.translateService.use('en');
      }
      });

      this.statusBar.styleDefault();
      this.splashScreen.hide();

      /* Commented code of force update*/
      this.user.checkVersion().then((dataSet)=>{
        if(dataSet['status']==true){
          var version=dataSet['data'].android_version;
          this.appVersion.getVersionNumber().then((s) => {
            // if(s!=version){
            //   this.nav.setRoot(ForceupdatePage);
            // }else
            // {
              this.storage.get('api_token').then((val) => {
              if(val != null && val != ''){
                this.nav.setRoot(DashboardPage);
              }else{
                this.nav.setRoot(TabsScreenPage);
              }
              }).catch((val)=>{
                this.nav.setRoot(TabsScreenPage);
              });
            //}
          });
        }
      });
    });
  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page.component){
      this.nav.setRoot(page.component);
    }else{
      this.storage.remove('api_token');
      this.storage.remove('image');
      this.storage.remove('mobile_no');
      this.storage.remove('profile');
      this.storage.remove('role');
      this.storage.remove('user_name');
      this.nav.setRoot(TabsScreenPage);
    }
  }
  ngDoCheck(){
  }
}
