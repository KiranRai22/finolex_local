import { HttpClient,HttpHeaders, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Headers,Http} from '@angular/http' ;
import 'rxjs/add/operator/toPromise' ;

/*
  Generated class for the RewardProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RewardProvider {

  _headers;
  constructor(public http: Http) {
    this._headers=new Headers();
    console.log('Hello ApisProvider Provider');
    this._headers.append('finolex-user','finolex@meltag.io');
    this._headers.append('finolex-password','finolex$meltag@123');

  }

  getRewardHistoryList(token,from,to)
  {
    // var _headers=new Headers();
    // console.log('Hello ApisProvider Provider');
    // this._headers.append('finolex-user','finolex@meltag.io');
    // this._headers.append('finolex-password','finolex$meltag@123');
    return new Promise((result,reject)=>{
        this.http.get('http://app.meltag.com/finolex/api/reward/history?api_token='+token+'&from_date='+from+'&to_date='+to , { headers : this._headers } ).toPromise()
        .then(res=>{
          result(res.json());

        },err=>{
          reject(err);
        });

    });


  }

  getRewardTypes(token,apicodes)
  {
    return new Promise((result,reject)=>{
      this.http.get('http://app.meltag.com/finolex/api/reward-types?api_token='+ token + '&codes=' + apicodes ,{headers : this._headers}).toPromise()
      .then(res=>{

        result(res.json());
      },err=>{
          reject(err);
      });
    
    });
  }

  changeRewardType(token,type){
  	
    return new Promise((result,reject)=>{

      this.http.post('http://app.meltag.com/finolex/api/reward-type/update?api_token='+ token + "&reward_type=" + type,'',{headers : this._headers}).toPromise()
      .then(res=>{
        result(res.json());
      },err=>{
        reject(err);
      })

    });
 }
}
