var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
/*
  Generated class for the RewardProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RewardProvider = /** @class */ (function () {
    function RewardProvider(http) {
        this.http = http;
        this._headers = new Headers();
        console.log('Hello ApisProvider Provider');
        this._headers.append('finolex-user', 'finolex@meltag.io');
        this._headers.append('finolex-password', 'finolex$meltag@123');
    }
    RewardProvider.prototype.getRewardHistoryList = function (token, from, to) {
        var _this = this;
        // var _headers=new Headers();
        // console.log('Hello ApisProvider Provider');
        // this._headers.append('finolex-user','finolex@meltag.io');
        // this._headers.append('finolex-password','finolex$meltag@123');
        return new Promise(function (result, reject) {
            _this.http.get('http://app.meltag.com/finolex/api/reward/history?api_token=' + token + '&from_date=' + from + '&to_date=' + to, { headers: _this._headers }).toPromise()
                .then(function (res) {
                result(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    RewardProvider.prototype.getRewardTypes = function (token, apicodes) {
        var _this = this;
        return new Promise(function (result, reject) {
            _this.http.get('http://app.meltag.com/finolex/api/reward-types?api_token=' + token + '&codes=' + apicodes, { headers: _this._headers }).toPromise()
                .then(function (res) {
                result(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    RewardProvider.prototype.changeRewardType = function (token, type) {
        var _this = this;
        return new Promise(function (result, reject) {
            _this.http.post('http://app.meltag.com/finolex/api/reward-type/update?api_token=' + token + "&reward_type=" + type, '', { headers: _this._headers }).toPromise()
                .then(function (res) {
                result(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    RewardProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], RewardProvider);
    return RewardProvider;
}());
export { RewardProvider };
//# sourceMappingURL=reward.js.map