import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Http,Headers} from '@angular/http';
/*
  Generated class for the ReedemapiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ReedemapiProvider {
_header;
  url:string='http://app.meltag.com/finolex/api/';
  constructor(public http: Http) {
    console.log('Hello ReedemapiProvider Provider');
    this._header=new Headers();
    this._header.append('finolex-user','finolex@meltag.io');
    this._header.append('finolex-password','finolex$meltag@123');
  }

  getReedemHistory(token,offset,from,to) {
  return new Promise((resolve,reject) => {
  	console.log(token);
    this.http.get(this.url+'redeem/history?api_token='+token+'&offset='+offset+'&limit=20&from_date='+from+'&to_date='+to,{headers:this._header}).toPromise().then(data => {
    	console.log(data);
     	resolve(data.json());
    }, err => {
     	console.log(err);
     	reject(err);
    });
  });
}
  getProductCatalogues(token){

      return new Promise((resolve,reject) => {
        console.log(token);
        this.http.get(this.url+'products?api_token='+token,{headers:this._header}).toPromise().then(data => {
          console.log(data);
           resolve(data.json());
        }, err => {
           console.log(err);
           reject(err);
        });
      });

    }

}
