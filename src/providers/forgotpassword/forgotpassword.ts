import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Http,Headers} from '@angular/http';

@Injectable()
export class ForgotpasswordProvider {
_header;
// url: "app.meltag.com/finolex/api/redeem/history?api_token=4fc04ae0-4c49-11e8-a623-711306335aaa";
url : "http://app.meltag.com/finolex/api/forgot-password";

  constructor(public http: Http) {

    console.log('Hello ReedemapiProvider Provider');
    this._header=new Headers();
    this._header.append('finolex-user','finolex@meltag.io');
    this._header.append('finolex-password','finolex$meltag@123');
    this._header.append('Access-Control-Allow-Origin' , '*');
    this._header.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

 }

 getForgotPassword(mobileno){

 /*const data=new FormData();
 data.append("mobile_no",mobileno);
   data.append("role",role);*/
   var data={
        'mobile_no' :mobileno
   };
   console.log(data);

	// let data=JSON.stringify({mobile_no:mobileno,role:"dealer"});

 	return new Promise((resolve,reject) => {
    this.http.post("http://app.meltag.com/finolex/api/forgot-password",data,{headers:this._header}).toPromise().then(data => {
    	console.log(data);
     	resolve(data.json());
    }, err => {
     	console.log(err);
     	reject(err);
    });
  });	
 }


 getOtp(mobileno,role){

   var data={
        'mobile_no' :mobileno,
        'role' : role
   };
   console.log(data);

  // let data=JSON.stringify({mobile_no:mobileno,role:"dealer"});

   return new Promise((resolve,reject) => {
    this.http.post("http://app.meltag.com/finolex/api/send-otp",data,{headers:this._header}).toPromise().then(data => {
      console.log(data);
       resolve(data.json());
    }, err => {
       console.log(err);
       reject(err);
    });
  });

 }

}
